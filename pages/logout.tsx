import React, { useContext, useEffect } from "react";

import { useRouter } from "next/router";

import { SessionContext } from "./_app";
import Loading from "@/components/Loading";

const Logout = () => {
  const { logout } = useContext(SessionContext);

  const router = useRouter();

  useEffect(() => {
    if (logout == null) return;
    logout();
    router.push("/");

    // this should happen only once after the component is mounted
    // and since we are changing the user it triggers logout recalculation so we cannot add logout as a dependency
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Loading />;
};
export default Logout;
