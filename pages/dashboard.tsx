import React, { useContext, useEffect, useState } from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import axios from "axios";

import { SessionContext } from "./_app";

import PageTitle from "@/components/PageTitle";
import Header from "@/components/Header";
import Loading from "@/components/Loading";
import Pagination from "@/components/Pagination";
import Post from "@/components/Post";
import { IPost } from "@/components/types";

const POSTS_PER_PAGE = 10;

const Dashboard: NextPage = () => {
  const router = useRouter();
  const { currentUser } = useContext(SessionContext);

  const [posts, setPosts] = useState<IPost[]>();
  const [pagePosts, setPagePosts] = useState<IPost[]>();
  const [error, setError] = useState<{ message: string }>();
  const [currentPage, setCurrentPage] = useState(1);

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_TYPICPODE_URL}/posts`)
      .then((response) => {
        setPosts(response.data);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  useEffect(() => {
    // checks if the user is authenticated
    if (currentUser?.token == null) {
      router.push("/login");
    }
  }, [currentUser?.token, router]);

  useEffect(() => {
    const indexOfLast = currentPage * POSTS_PER_PAGE;
    const indexOfFirst = indexOfLast - POSTS_PER_PAGE;

    // since the api does not have pagination
    // we need to split the posts into pages
    setPagePosts(posts?.slice(indexOfFirst, indexOfLast));
  }, [currentPage, posts]);

  if (currentUser?.token == null) return <Loading />;

  return (
    <div className="flex flex-col items-center">
      <Header />
      <div
        className="flex flex-col items-center p-4 mt-20"
        style={{ minHeight: 600, maxWidth: 1000 }}
      >
        <PageTitle title="Dashboard" />

        <div className="p-2" />

        <div className="grid grid-cols-3 gap-4 mb-4">
          {pagePosts?.map((post) => (
            <Post key={post.id} content={post} />
          ))}
        </div>

        {error != null && (
          <>
            <p>There was an error, please try again!</p>
            <p>{error.message}</p>
          </>
        )}

        <div className="p-8" />

        <div className="max-w-lg">
          <Pagination
            paginate={paginate}
            currentPage={currentPage}
            pagesCount={(posts ?? []).length / POSTS_PER_PAGE}
          />
        </div>

        <div className="p-2" />
      </div>
    </div>
  );
};

export default Dashboard;
