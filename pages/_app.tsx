import { createContext, useCallback, useMemo } from "react";
import type { AppProps } from "next/app";

import { useLocalStorage } from "@/components/utils";
import Footer from "@/components/Footer";

import "@/styles/globals.css";

type SessionContext = {
  currentUser?: { username?: string; token?: string };
  login?: (session: { token: string; username: string }) => void;
  logout?: () => void;
};

export const SessionContext = createContext<SessionContext>({});

export default function MyApp({ Component, pageProps }: AppProps) {
  const [user, setUser, removeUser] = useLocalStorage<
    SessionContext["currentUser"]
  >({ key: "user" });

  const login = useCallback(
    (session: { token: string; username: string }) => {
      setUser(session);
    },
    [setUser]
  );

  const contextValue = useMemo(
    () => ({
      currentUser: user,
      login,
      logout: removeUser,
    }),
    [login, removeUser, user]
  );

  return (
    <SessionContext.Provider value={contextValue}>
      <Component {...pageProps} />
      <Footer />
    </SessionContext.Provider>
  );
}
