import React, { useEffect, useState } from "react";
import { NextPage } from "next";
import Link from "next/link";
import axios from "axios";

import Post from "@/components/Post";
import PageTitle from "@/components/PageTitle";
import Header from "@/components/Header";
import { IPost } from "@/components/types";

const Home: NextPage = () => {
  const [posts, setPosts] = useState<IPost[]>();
  const [error, setError] = useState<{ message: string }>();

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((response) => {
        setPosts(response.data);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  return (
    <div className="flex flex-col items-center">
      <Header />
      <div className="p-4 mt-20" style={{ minHeight: 700, maxWidth: 800 }}>
        <PageTitle title="Welcome to Space Instagram" />

        <div className="p-2" />

        <main>
          <section>
            <p className="text-gray-700">
              We bring you the most breathtaking posts of our universe, captured
              by the most advanced scientists, space telescopes and probes.
              Follow us and get inspired by the awe-inspiring beauty of the
              cosmos.
            </p>
          </section>

          <div className="p-2" />

          <section>
            <div className="flex flex-col items-center gap-4">
              <h2 className="text-gray-500 font-semibold">Latest Posts:</h2>

              {posts?.slice(0, 2).map((post) => (
                <Post key={post.id} content={post} />
              ))}

              {error != null && (
                <>
                  <p>There was an error, please try again!</p>
                  <p>{error.message}</p>
                </>
              )}

              <Link
                data-testid="see-more-posts"
                className="bg-orange-400 border border-orange-500 hover:border-orange-300 hover:bg-orange-300 text-white rounded-md px-4 py-3"
                href="/dashboard"
              >
                See more posts
              </Link>
            </div>
          </section>
        </main>
      </div>
    </div>
  );
};

export default Home;
