import React, { useContext, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import Link from "next/link";

import { SessionContext } from "./_app";
import users from "@/users.json";

import Logo from "@/assets/logo.png";

const Login = () => {
  const { currentUser, login } = useContext(SessionContext);

  const router = useRouter();

  // its easier to manage a single state object in this case
  const [state, setState] = React.useState<{
    username?: string;
    password?: string;
    signedUp?: boolean;
    emailError?: boolean;
    notFoundError?: boolean;
  }>({ emailError: false, notFoundError: false });

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (state.username == null || state.password == null) {
      setState({ ...state, emailError: true });
      return;
    }

    const isEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
      state.username
    );

    if (isEmail !== true) {
      setState({ ...state, emailError: true });
    }

    const existingUser = users.find((user) => user.email === state.username);
    const existingUserPasswordMatch = existingUser?.password === state.password;

    if (existingUserPasswordMatch === true) {
      // we would normally make a request the token to the server and set it here
      // so that we can use it in the header of the requests
      login?.({ token: "yay", username: state.username });
    } else {
      setState({ ...state, notFoundError: true });
    }
  };

  useEffect(() => {
    // redirect to dashboard when user is logged in
    if (currentUser?.token != null) {
      router.push("/dashboard");
    }
  }, [currentUser?.token, router]);

  return (
    <div className="max-w-2xl mx-auto" style={{ minHeight: 700 }}>
      <div className="flex flex-col justify-center items-center">
        <Link href="/">
          <Image src={Logo} className="m-8 w-32 h-auto" alt="Logo" />
        </Link>

        <form onSubmit={onSubmit} className="flex flex-col">
          <label htmlFor="username">Username</label>
          <div className="p-1" />

          <input
            type="text"
            required
            className="bg-transparent border rounded border-gray-400 p-1"
            placeholder="E.g. user1@sw.com"
            id="username"
            defaultValue={state.username}
            onChange={(e) =>
              setState({
                ...state,
                username: e.target.value,
                emailError: false,
                notFoundError: false,
              })
            }
          />

          <div className="p-2" />

          <label htmlFor="username">Password</label>
          <div className="p-1" />

          <input
            type="password"
            required
            className="bg-transparent border rounded border-gray-400 p-1"
            placeholder="*******"
            id="password"
            defaultValue={state.password}
            onChange={(e) =>
              setState({
                ...state,
                password: e.target.value,
                emailError: false,
                notFoundError: false,
              })
            }
          />
          <div className="p-1" />

          {state.emailError === true && (
            <>
              <small className="text-red-500">
                Username must be a valid email
              </small>
            </>
          )}
          {state.notFoundError === true && (
            <>
              <small className="text-red-500">
                Username or password is incorrect
              </small>
            </>
          )}

          <div className="p-2" />

          <input
            type="submit"
            className={`bg-blue-700 hover:bg-blue-500 border text-white border-blue-800 hover:border-blue-500 rounded p-1 ${
              state.notFoundError === true
                ? "animate-bounce cursor-not-allowed"
                : ""
            }`}
            value="Login"
          />
          <div className="p-2" />
        </form>
      </div>
    </div>
  );
};

export default Login;
