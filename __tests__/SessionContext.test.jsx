import React from "react";
import "@testing-library/jest-dom";
import { render, cleanup, fireEvent } from "@testing-library/react";

import { SessionContext } from "@/pages/_app";
import Home from "@/pages/index";

const renderWithContext = (component) => {
  return {
    ...render(
      <SessionContext.Provider value={SessionContext}>
        {component}
      </SessionContext.Provider>
    ),
  };
};

afterEach(cleanup);

describe("Home", () => {
  it("checks if initial state is signed out", () => {
    const { getByTestId } = renderWithContext(<Home />);
    expect(getByTestId("sign-in")).toBeVisible();
  });
});
