import "@testing-library/jest-dom";
import { render, cleanup, screen } from "@testing-library/react";

import Home from "../pages/index";

afterEach(cleanup);

describe("Home", () => {
  it("renders a landing page with a title", async () => {
    render(<Home />);

    expect(screen.getByTestId("title")).toBeVisible();
    expect(screen.getByTestId("title")).toBeVisible();

    const linkElement = screen.getByText(/see more/i);
    expect(linkElement).toBeInTheDocument();
  });
});
