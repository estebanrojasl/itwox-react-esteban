import mockAxios from "jest-mock-axios";
import axios from "axios";

const fetchPosts = async () =>
  axios
    .get(`${process.env.NEXT_PUBLIC_TYPICPODE_URL}/posts`)
    .then((response) => response)
    .catch((error) => error);

describe("fetch posts", () => {
  afterEach(() => {
    mockAxios.reset();
  });

  describe("API call is successful", () => {
    it("should return posts list", async () => {
      // given
      const posts = [
        { id: 1, title: "test title", body: "test body", userId: 1 },
        { id: 2, title: "test title", body: "test body", userId: 1 },
      ];
      mockAxios.get.mockResolvedValueOnce(posts);

      // when
      const result = await fetchPosts();

      // then
      expect(mockAxios.get).toHaveBeenCalledWith(
        `${process.env.NEXT_PUBLIC_TYPICPODE_URL}/posts`
      );
      expect(result).toEqual(posts);
    });
  });
});
