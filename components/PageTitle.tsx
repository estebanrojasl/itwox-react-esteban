import React from "react";

const PageTitle = ({ title }: { title?: string }) => {
  return (
    <h1
      data-testid="title"
      className="text-3xl font-bold h-10 text-orange-500 self-start"
    >
      {title == null ? (
        <div className="animate-pulse rounded w-32 h-6 bg-slate-200" />
      ) : (
        <span>{title}</span>
      )}
    </h1>
  );
};

export default PageTitle;
