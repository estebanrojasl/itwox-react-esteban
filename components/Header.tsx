import React, { useContext } from "react";
import Link from "next/link";
import Image from "next/image";

import { SessionContext } from "@/pages/_app";

import Logo from "@/assets/logo.png";
import Avatar from "@/assets/avatar.png";

const Header = () => {
  const { currentUser } = useContext(SessionContext);

  return (
    <>
      <nav
        data-testid="header"
        className="fixed top-0 z-50 w-full bg-white border-b border-gray-200 h-18"
      >
        <div className="px-3 py-3">
          <div className="flex items-center justify-between">
            <Link href="/" className="flex ml-2 h-12">
              <Image src={Logo} className="max-h-12 w-auto" alt="Logo" />
            </Link>

            <div className="flex items-center justify-center">
              {currentUser?.token != null ? (
                <ul className="flex flex-row">
                  <li>
                    <Link
                      href="/dashboard"
                      className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    >
                      Dashboard
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="/logout"
                      className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    >
                      Log out
                    </Link>
                  </li>
                </ul>
              ) : (
                <Link
                  data-testid="sign-in"
                  href="/login"
                  className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                >
                  Sign in
                </Link>
              )}
            </div>

            <div className="flex justify-center items-center h-12">
              <small className="text-gray-500">{currentUser?.username}</small>
              <div className="p-2" />
              <Image
                className="w-10 h-10 rounded-full"
                src={Avatar}
                alt="user photo"
              />
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
