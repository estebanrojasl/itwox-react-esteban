import React, { useEffect, useState } from "react";
import Image from "next/image";
import axios from "axios";

import { IPost } from "@/components/types";
import Loading from "./Loading";

import Avatar from "@/assets/comment-avatar.png";

type Comment = {
  id: number;
  body: string;
};

const CommentLine = ({ content }: { content: string }) => {
  const [showMore, setShowMore] = useState(false);

  return (
    <button
      className="text-sm font-normal text-left text-gray-500"
      onClick={() => setShowMore(true)}
    >
      {showMore !== true ? `${content.substring(0, 50)}...` : content}
    </button>
  );
};

const Comments = ({ postId }: { postId: number }) => {
  const [comments, setComments] = useState<Comment[]>();
  const [showMore, setShowMore] = useState(false);

  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_TYPICPODE_URL}/posts/${postId}/comments`)
      .then((response) => {
        setComments(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  if (comments == null) return <Loading />;

  return (
    <>
      {showMore === true ? (
        <>
          <p>Comments:</p>
          <ul>
            {comments?.map(({ id, body }) => (
              <li key={id} className="mb-2">
                <CommentLine content={body} />
              </li>
            ))}
          </ul>
        </>
      ) : (
        <button className="font-normal" onClick={() => setShowMore(true)}>
          <small>View all {comments.length} comments</small>
        </button>
      )}
    </>
  );
};

const Post = ({ content }: { content: IPost }) => {
  const { id, title, body } = content;
  return (
    <div
      data-testid="post"
      className="w-full max-w-md p-8 bg-white border border-gray-200 rounded-lg shadow"
    >
      <div className="flex flex-col items-start justify-center">
        <div className="flex flex-row">
          <div className="flex-shrink-0 self-start">
            <Image className="w-8 h-8 rounded-full" src={Avatar} alt="avatar" />
          </div>
          <div className="p-2" />
          <div className="flex-1 min-w-0">
            <h2 className="text-orange-500  font-semibold capitalize">
              {title}
            </h2>
          </div>
        </div>

        <div className="p-2" />

        <div className="flex flex-col">
          <p className="text-left text-gray-600">{body}</p>

          <div className="p-2" />

          <div className="items-center text-base font-semibold text-gray-600">
            <Comments postId={id} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Post;
